package com.subrutin.catalog.dto;

import java.io.Serializable;


import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Data
public class CategoryCreateUpdateRequestDTO implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 2877529575926259287L;
	
	@NotBlank
	private String code;
	
	@NotBlank
	private String name ;
	
	private String description ;
	
	

}
