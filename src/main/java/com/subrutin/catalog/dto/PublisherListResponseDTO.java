package com.subrutin.catalog.dto;

import java.io.Serializable;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;


@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PublisherListResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7061299583772571511L;
	
	private String publisherId;
	
	private String publisherName;
	
	private String companyName;

}
