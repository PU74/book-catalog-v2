package com.subrutin.catalog.security.model;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class JwtAuthenticationToken extends AbstractAuthenticationToken{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8898088561242122898L;

	private RawAccessJwtToken rawAccessJwtToken;
	
	private UserDetails userDetails;
	
	public JwtAuthenticationToken(RawAccessJwtToken token) {
		super(null);
		this.rawAccessJwtToken =  token ;
		super.setAuthenticated(false);
	}

	public JwtAuthenticationToken(UserDetails userDetails, Collection<? extends GrantedAuthority> authorities) {
		super(authorities);
		this.eraseCredentials();
		this.userDetails = userDetails;
		super.setAuthenticated(true);
		// TODO Auto-generated constructor stub
		
	}


	@Override
	public Object getCredentials() {
		// TODO Auto-generated method stub
		return this.rawAccessJwtToken;
	}

	@Override
	public Object getPrincipal() {
		// TODO Auto-generated method stub
		return this.userDetails;
	}


	@Override
	public void eraseCredentials() {
		// TODO Auto-generated method stub
		super.eraseCredentials();
		this.rawAccessJwtToken = null;
	}

}
