package com.subrutin.catalog.security.model;


import org.springframework.security.authentication.AbstractAuthenticationToken;


public class AnonymousAuthentication extends AbstractAuthenticationToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = -19773817651656000L;

	public AnonymousAuthentication() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object getCredentials() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getPrincipal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAuthenticated() {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
