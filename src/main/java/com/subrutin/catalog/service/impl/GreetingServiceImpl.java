package com.subrutin.catalog.service.impl;

import java.util.TimeZone;

import org.springframework.stereotype.Service;

import com.subrutin.catalog.config.ApplicationProperties;
import com.subrutin.catalog.config.CloudProperties;
import com.subrutin.catalog.service.GreetingService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@Service
public class GreetingServiceImpl implements GreetingService {
	
//ini sudah tidak dipakai lagi karna sudah menggunakan spring configuration properties dimana objek2 dibawah ini sudah dibuat di file ApplicationProperties.java
//	@Value("${welcome.text}")
//	private String welcomeText;
//	
//	@Value("${timezone}")
//	private String timeZone;
//	
//	@Value("${currency}")
//	private String currency;
	
	
	private ApplicationProperties appProperties;
	
	private CloudProperties cloudProperties;

	@Override
	public String sayGreeting() {
		log.trace("ini log trace");
		log.debug("ini log debug");
		log.warn("ini log warn");
		log.info("ini log info");
		log.error("ini log error");
		System.out.println(cloudProperties.getApiKey());
		TimeZone timezone= TimeZone.getTimeZone(appProperties.getTimezone());
		return appProperties.getWelcomeText() +" Our Time Zone : "+timezone.getDisplayName()+", Our Currency : "+appProperties.getCurrency();
		
	}
}
