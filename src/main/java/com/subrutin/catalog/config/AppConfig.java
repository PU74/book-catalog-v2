package com.subrutin.catalog.config;

import java.security.Key;

import javax.crypto.SecretKey;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.subrutin.catalog.security.util.JwtTokenFactory;

import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;


@Configuration
public class AppConfig {
	
	@Bean
	public SecretKey key() {
		byte[] keyBytes = Decoders.BASE64.decode("asKdkljefelsfoeflsfmsndklasdjalsdjsalawdmnciospmsdfsfokesfso");
		return Keys.hmacShaKeyFor(keyBytes);
	}
	
	@Bean
	public JwtTokenFactory jwtTokenFactory(Key key) {
		return new JwtTokenFactory(key);
	}
	@Bean 
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}
	//Khusus Java Based Config
//	@Bean
//	public Author author() {
//		return new Author(1L,"Puja Wijaksono",null, false);
//	}
//
//	@Bean
//	public Book book1(Author author) {
//		Book book=new Book();
//		book.setId(1L);
//		book.setTitle("Bumi manusia");
//		book.setDescription("Bumi Manusia karya Puja");
//		book.setAuthor(author);
//		return book;
//	}
//	
//	@Bean
//	public Book book2(Author author) {
//		Book book=new Book();
//		book.setId(2L);
//		book.setTitle("Arok Dedes");
//		book.setDescription("Arok Dedes karya Pramudya");
//		book.setAuthor(author);
//		return book;
//	}
//	
//	@Bean
//	public BookRepository bookRepository(Book book1, Book book2) {
//		Map<Long, Book> bookMap=new HashMap<Long, Book>();
//		bookMap.put(1L, book1);
//		bookMap.put(2L, book2);
//		
//		BookRepositoryImpl bookRepository = new BookRepositoryImpl();
//		bookRepository.setBookMap(bookMap);
//		return bookRepository;
//		
//	}
	
//	@Bean
//    public BookService bookService(BookRepository bookRepository) {	
//		return new BookServiceImpl(bookRepository);
//	}
}
