package com.subrutin.catalog.config.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

	
//	Cara lama 
//	@RequestMapping(value="/home", method=RequestMethod.GET)
	//Cara Baru
	@GetMapping("/home")
	public String Home(Model model) {
		model.addAttribute("name", "Puja");
		return "home";
	}
}
